"use strict";
// ## Теоретичне питання
// Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію `try...catch`.

// Конструкція try...catch використовується для обробки винятків (виключень) у JavaScript:

//Обробка некоректного введення користувача:
// try {
//   const userInput = getUserInput();
//   if (!validateInput(userInput)) {
//     throw new Error("Некоректний ввід");
//   }
// } catch (error) {
//   console.error("Помилка обробки введення:", error.message);
// }

// Перевірка існування об'єктів чи властивостей:
// try {
//   const value = object.property; // може викликати помилку, якщо property не існує
// } catch (error) {
//   console.error("Помилка при доступі до властивості:", error.message);
// }

// Виклик функції, яка може викинути помилку:
// try {
//   performPotentiallyRiskyOperation();
// } catch (error) {
//   console.error("Помилка при виконанні операції:", error.message);
// }

// ## Завдання
// Дано масив books.
// - Виведіть цей масив на екран у вигляді списку (тег ul – список має бути згенерований за допомогою Javascript).
// - На сторінці повинен знаходитись `div` з `id="root"`, куди і потрібно буде додати цей список (схоже завдання виконувалось в модулі basic).

// - Ті елементи масиву, які не є коректними за умовами попереднього пункту, не повинні з'явитися на сторінці.

const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

// - Перед додаванням об'єкта на сторінку потрібно перевірити його на коректність (в об'єкті повинні міститися всі три властивості - author, name, price). Якщо якоїсь із цих властивостей немає, в консолі має висвітитися помилка із зазначенням - якої властивості немає в об'єкті.
const root = document.querySelector(`#root`);
const list = document.createElement(`ul`);
root.append(list);

books.forEach((item) => {
  try {
    if (
      item.author === undefined ||
      item.name === undefined ||
      item.price === undefined
    ) {
      const requiredProperties = ["author", "name", "price"];
      const missingProperties = [];

      requiredProperties.forEach((property) => {
        if (item[property] === undefined) {
          missingProperties.push(property);
        }
      });

      if (missingProperties.length > 0) {
        throw new Error(
          `В об'єкті не вистачає властивості: ${missingProperties.join(", ")}`
        );
      }
    }
    const listItem = document.createElement(`li`);
    listItem.textContent = `Автор: ${item.author}. Назва: ${item.name}. Ціна: ${item.price}`;
    list.appendChild(listItem);
  } catch (error) {
    console.error(error.message);
  }
});
